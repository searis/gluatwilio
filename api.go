package gluatwilio

import (
	"fmt"

	twilio "github.com/carlosdp/twiliogo"
	"github.com/yuin/gopher-lua"
)

var api = map[string]lua.LGFunction{
	"sendSMS": apiSendSMS,
}

func apiSendSMS(L *lua.LState) int {
	number := L.CheckString(1)
	msg := L.CheckString(2)

	fmt.Printf("Sending sms to %s with content: %s\n", number, msg)
	message, err := twilio.NewMessage(client, "+19854675006", number, twilio.Body(msg))

	if err != nil {
		fmt.Println(err.Error())
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
		return 2
	}

	fmt.Println(message.Status)
	L.Push(lua.LString("ok"))

	return 1
}
