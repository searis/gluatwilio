package gluatwilio

import (
	"testing"

	"github.com/yuin/gopher-lua"
)

func TestSimple(t *testing.T) {
	const str = `
	local twilio = require("twilio")
	assert(type(twilio) == "table")
	assert(type(twilio.sendSMS) == "function")

	assert(twilio.sendSMS("+4791586531", "Banan") == "ok")
	`
	s := lua.NewState()
	Preload(s, "AC11a392e1837a9eb4e397bc0e1b58a693", "bb6e486a116770190f267418b1b68b91")
	if err := s.DoString(str); err != nil {
		t.Error(err)
	}
}
