package gluatwilio

import (
	twilio "github.com/carlosdp/twiliogo"
	"github.com/yuin/gopher-lua"
)

var client twilio.Client

// Preload adds twilio to the given Lua state's package.preload table. After it
// has been preloaded, it can be loaded using require:
//
//  local twilio = require("twilio")
func Preload(L *lua.LState, sid, token string) {
	client = twilio.NewClient(sid, token)
	L.PreloadModule("twilio", Loader)
}

// Loader is the module loader function.
func Loader(L *lua.LState) int {
	t := L.NewTable()
	L.SetFuncs(t, api)
	L.Push(t)
	return 1
}
